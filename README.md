# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

A prototype Customer Service System was developed using Java language. This system provided the GUI interactivity that can be used in the environment of a telecommunication company’s call centre. This system provided three views working at the same time:
First View: Call Centre Consultant (CCC) – the CCC receives the call for help from the customer, logs the call as a job, and if the problem can not be resolved immediately, leaves the problem in the system as a job to be handled by a TSO. All resolved and unresolved jobs are saved in separate files.

Second View: Technical Support Officer (TSO) – the TSO handles the jobs in the CSS, in the order that they come.

Third View: Customer Support Manager (CSM) – the CSM has oversight of the CCCs and TSOs. The CSM can advance jobs for faster attention by the TSOs. 
Operations Manager (OM) – the OM has oversight of all other roles, but is not involved directly in customer service. The OM is interested in the performance of the company and the system, and needs relevant statistical reports. 

### How do I run it

Download java. Run through command line.