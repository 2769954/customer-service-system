/*
** file:    JobOpen.java
** author:  Usman Waqas
** purpose: jobs records.
*/

import java.util.*;
import java.io.*;


public class JobOpen {

      private int id; // job id number
      private static int next_id=0;//next job number
      private long phone;//phone number
      private String name; // Customer Name
      private String detail; //job details
      private int job_length;//number of lines of job description
      private Date date = new Date();//for start time
      private long time;
      private long time2;
      private ArrayList<String> jobsSplitLines = new ArrayList<String>(); // job deatl split by "/n"
    
       // new JobOpen(String newName, long phone, String detail) makes a new job
       // with this id and name.
      public JobOpen(String newName, long phone, String detail){
          this.id = next_id;
          this.phone = phone;
          this.name = newName.trim();
          this.detail = detail.trim();
          this.time = date.getTime();
          this.time2 = date.getTime();
          String[] arr = detail.split("\n");
          ArrayList<String> y = new ArrayList<String>(Arrays.asList(arr));
          jobsSplitLines = y;
          this.next_id = next_id+1;
      } 
         
        //Load from file
      public JobOpen(int id,String name,long phone,int line,ArrayList<String> jobDetail,long time){
          this.id = id;
          this.name = name;
          this.phone = phone;
          this.job_length = line;
          jobsSplitLines = jobDetail;
          this.time = time;
      }
        
      public void setTime2(){
          time2 = date.getTime();
      }
       
      public static void setNextId(int e){
          next_id = e;
      }
       
      public int getId() {
          return id;
      }
       
      public long getPhone() {
          return phone;
      }
       
      public String getName(){
          return name;
      }
       
       //sends list with job description
      public ArrayList<String> getDescrip(){
         return jobsSplitLines;
      }
       
      public long getTime() {
          return time;
      }
       
      public long getTime2() {
          return time2;
      }
       
      // send total number of lines of job description
      public int getL() {
          job_length = jobsSplitLines.size();
          return job_length;
      }
       
      //next id
      public static int getNext() {
          return next_id;
      }
   
   }
