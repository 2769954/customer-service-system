/*
** file:    Action.java
** author:  Usman Waqas
** purpose: Class to perform all the actions required by CUstomer Service System
*/

import java.util.*;
import java.io.*;

public class Action{
   
      private static List<JobOpen> pendingJobs = new ArrayList<JobOpen>();//open jobs list
      private static List<JobOpen> resolvedJobs = new ArrayList<JobOpen>();//completed jobs list
      
      // load(fileName) loads the un-resolved jobs left from the
      // previous session.
      public void loadOpen(String fileName) {
         try {
            Scanner in = new Scanner(new File(fileName));
            JobOpen.setNextId(in.nextInt());
            while (in.hasNextInt()) {
            int id = in.nextInt();
            String name = in.next();
            long phone = in.nextLong();
            int line = in.nextInt();
            ArrayList<String> jobDetail = new ArrayList<String>();//adds job description
               for(int j=0;j<line;j=j+1){
                  jobDetail.add(in.next());
               }
            long time= in.nextLong();	
            pendingJobs.add(new JobOpen(id,name,phone,line,jobDetail,time));
            }
            in.close();
         } catch (Exception e) {
            // skip (there was no data file)
         }
      }  
      
      // saveOpen(fileName) saves the Jobs for
      // next session.
      public void saveOpen(String fileName) {
         try {
            PrintStream out =new PrintStream(fileName);
            out.println(JobOpen.getNext());
            for (int s=0; s<pendingJobs.size(); s=s+1) {
               JobOpen g = pendingJobs.get(s);
               out.println(g.getId());
               out.println(g.getName());
               out.println(g.getPhone());
               out.println(g.getL());
               List<String> arr=g.getDescrip();
               for(int m=0; m<arr.size(); m=m+1){//job description
               out.println(arr.get(m));
               }
               out.println(g.getTime());
            }
            out.close();
         } catch (Exception e) {
            System.err.println("Could not save " + 
                               fileName);
            System.err.println(e);
         }
      }
      
      //function to save to closefile jobs. 
      public void saveClose(String fileName2) {
         try {
            PrintStream out =new PrintStream(new FileOutputStream(fileName2,true));
            for (int s=0; s<resolvedJobs.size(); s=s+1) {
               JobOpen job= resolvedJobs.get(s);
               out.println(job.getId());
               out.println(job.getName());
               out.println(job.getPhone());
               out.println(job.getL());
               List<String> jobDetail = job.getDescrip();
               for(int m=0; m<jobDetail.size(); m=m+1)//job description
               {
                   out.println(jobDetail.get(m));
               }
                out.println(job.getTime());
                out.println(job.getTime2());
            }
            out.close();
         } catch (Exception e) {
            System.err.println("Could not save " + 
                               fileName2);
            System.err.println(e);
         }
      }
      
      //add job to un-resolved joblist
      public int addOpen(String newName, long phone, String description){
         JobOpen jobNew = new JobOpen(newName, phone, description);
         pendingJobs.add(jobNew);
         return jobNew.getId();
      }
       
        //add job to resolved joblist
      public JobOpen addClose(String name,int p,String descrip){
         JobOpen resolved = new JobOpen(name,p,descrip);
         resolvedJobs.add(resolved);
         return resolved;
      }
       
      //TSO resolves first job in the qeue 
      public void removeOpen(){ 
         try{
             JobOpen v = pendingJobs.get(0);
             v.setTime2();//call finish time
             resolvedJobs.add(v);//add to completed jobs list
             pendingJobs.remove(0);
         }
         catch(Exception e){
             System.err.println("No job to resolve  "+ e);
         }	
      }
       
      //prioritise a job for Manager
      public void priority(String m){
           try{
               LinkedList<JobOpen> link=new LinkedList<JobOpen>(pendingJobs);
               int d=Integer.parseInt(m);
               int size=link.size();
               ListIterator<JobOpen> it=link.listIterator(0);
               for(int i=0;i<size;i=i+1){
                if(link.get(i).getId()==d)
                   {
                       link.add(0,link.get(i));
                       link.remove(i+1);
                   }
                   else{
                   //Do Nothing
                   }
                   pendingJobs=link;
               }
           }
           catch(Exception e){ // handle your exception
                  System.err.println(e);
                }		
       }
       
       //Job detail to be shown all three windows
      public String toString(){
         StringBuilder b = new StringBuilder();
         for (JobOpen e : pendingJobs) {
            b.append("\n");
            b.append("\n");
            b.append("**************** NEW JOB ***************************");
            b.append("\n");
            b.append("Job Number: ");
            b.append(e.getId()).append("\n");
            b.append("Customer Name: ");
            b.append(e.getName()).append("\n");
            b.append("Phone #: ");
            b.append(e.getPhone()).append("\n");
            //b.append(e.getL()).append("\n");
            // String[] arr=e.getDescrip();
            b.append("Job Query : ");
            List<String> arr=e.getDescrip();
               for(int m=0;m<arr.size();m=m+1){//job description
               b.append(arr.get(m)).append("\n");
            }
         }
         return b.toString();
      }
      
      //send list to form JList 
      public String[] getList(){
         String[] arr = new String[pendingJobs.size()];
         int size = pendingJobs.size();
         for(int i=0;i<size;i=i+1){
           arr[i]=String.format("%d", pendingJobs.get(i).getId());
         }
         return arr;
      }
      
      int getNextID(){
           int m = pendingJobs.size();
           return pendingJobs.get(m).getNext();
      }
  
   }


