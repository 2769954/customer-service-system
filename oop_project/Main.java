/*
**Author: Usman Waqas
** file:  Main.java
** purpose: Puts up three window frames .
*/


import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;

public class Main {

      // openjobs.text contains all the unresolved jobs
      private static final String FILE_NAME = "openjobs.txt";
      // closejobs.text contains all the unresolved jobs
      private static final String FILE_NAME2 = "closejobs.txt"; 
      private static Action action = new Action(); 
      private static JFrame frame = new JFrame("Customer Centre Consultant");
      private static JFrame frame1 = new JFrame("Technical Support Office");
      private static JFrame frame2 = new JFrame("Customer Support Manager");
      private static DefaultListModel<String> listModel = new DefaultListModel<String>();
      private static JList<String>  mylist=new JList<String>(listModel); //Jlist for CCM window
     
      
      public static void main(String[] args) {
         action.loadOpen(FILE_NAME);
         String[] jobIdList = action.getList();
         //fill up Jlist
         for(int i=0;i<jobIdList.length;i=i+1){
          listModel.add(i, jobIdList[i]);
         }
         layoutComponentsCCC();
         layoutComponentsTSO();
         layoutComponentsCSM();
         addListenersCCC();
         addListenersTSO();
         addListenersCSM();
         frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE);
         frame1.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE);
         frame2.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE);
         frame.pack();
         frame.setVisible(true);
         frame1.pack();
         frame1.setVisible(true); 
         frame2.pack();
         frame2.setVisible(true);
         frame1.setLocation(350,0);	
         frame2.setLocation(750,0);
      }
      
         // input text fields for Customer Care Consultatnt
         private static JTextField 
                       name =  new JTextField(20),//name of the customer
                       phoneNumber =  new JTextField(20);
         private static JTextArea jobDetail =  new JTextArea(8, 20);//Job Description
         // buttons for CCC:
         private static JButton
                       resolveBtn = new JButton("Resolve"),
                       leaveBtn = new JButton("Leave");
         
         //TSO input and output fields
         private static JTextArea jobDetailTso =  new JTextArea(10, 25);// Output text area for TSO:
         //buttons for TSO
         private static JButton resolveBtn2 = new JButton("Resolve");
                      
         //CSM input Output textfields
         private static JTextArea jobDetailCsm =  new JTextArea(10, 25);//show open job list
         private static JButton saveBtn = new JButton("Save");
         private static JButton priorityBtn = new JButton("Give Priority");
     
      // layoutComponentsCCC() lays out the components in Customer Care Consultant.
      private static void layoutComponentsCCC() {
         JPanel box0 = new JPanel(),
                box1 = new JPanel(),
                box2 = new JPanel(),
                box3 = new JPanel(),
                box4 = new JPanel();
         box0.setLayout(new BoxLayout(box0,
            BoxLayout.Y_AXIS));
         box1.setLayout(new BoxLayout(box1,
            BoxLayout.X_AXIS));
         box2.setLayout(new BoxLayout(box2,
            BoxLayout.X_AXIS));
         box3.setLayout(new BoxLayout(box3,
            BoxLayout.X_AXIS));
         box4.setLayout(new BoxLayout(box4,
            BoxLayout.X_AXIS));
         frame.add(box0);
         JLabel labeln = new JLabel(" Enter New Job ");
         labeln.setFont(new Font("Serif", Font.PLAIN, 14)); 
         labeln.setForeground(Color.BLACK);
         box0.add(labeln);
         box0.add(box1);
         box0.add(box2);
         box0.add(box3);
         box0.add(box4);
         box1.setAlignmentX(Component.CENTER_ALIGNMENT);
         box2.setAlignmentX(Component.CENTER_ALIGNMENT);
         box3.setAlignmentX(Component.CENTER_ALIGNMENT);
         box1.setBorder(new EmptyBorder(5, 5, 5, 5));
         box2.setBorder(new EmptyBorder(5, 5, 5, 5));
         box3.setBorder(new EmptyBorder(5, 5, 5, 5));
         box4.setBorder(new EmptyBorder(5, 5, 5, 5));
         JLabel n = new JLabel("Customer Name:");
         n.setFont(new Font("Serif", Font.PLAIN, 14));
         box1.add(n);
         box1.add(name);
         JLabel r = new JLabel("Contact Number:");
         r.setFont(new Font("Serif", Font.PLAIN, 14));
         box2.add(r);
         box2.add(phoneNumber);
         JLabel rlabel = new JLabel("Job Description: ");
         rlabel.setForeground(Color.BLACK);
         rlabel.setFont(new Font("Serif", Font.PLAIN, 14));
         box3.add(rlabel);
         rlabel.setAlignmentX(Component.LEFT_ALIGNMENT);
         JScrollPane scroller = new JScrollPane(jobDetail);
         box3.add(scroller);
         scroller.setAlignmentX( Component.LEFT_ALIGNMENT);
         box4.add(resolveBtn);
         box4.add(leaveBtn);
      }
      
       // layoutComponentsTSO() lays out the components in TSO Window.
      private static void layoutComponentsTSO() {
         JPanel box0 = new JPanel(),
                box1 = new JPanel(),
                box2 = new JPanel(),
                box3 = new JPanel();
               
         box0.setLayout(new BoxLayout(box0,
            BoxLayout.Y_AXIS));
         box1.setLayout(new BoxLayout(box1,
            BoxLayout.Y_AXIS));
         box3.setLayout(new BoxLayout(box3,
            BoxLayout.X_AXIS));
         frame1.add(box0);
         box0.add(box1);
         box0.add(box3);
         box1.setAlignmentX(Component.CENTER_ALIGNMENT);
         box3.setAlignmentX(Component.CENTER_ALIGNMENT);
         box1.setBorder(new EmptyBorder(15, 15,15, 15));
         box3.setBorder(new EmptyBorder(5, 5, 5, 5));
         JLabel label2 = new JLabel("Un-resolved Jobs:");
         label2.setForeground(Color.BLACK);
         label2.setFont(new Font("Serif", Font.PLAIN, 14));
         box1.add( label2);
         box1.add(jobDetailTso);
         box3.add(resolveBtn2);
         jobDetailTso.setEditable(false);
         jobDetailTso.setText(action.toString());
         JScrollPane scroller = new JScrollPane(jobDetailTso);
         box1.add(scroller);
        scroller.setAlignmentX( Component.LEFT_ALIGNMENT);
      }
      
      //// layoutComponentsCSM() lays out the components in CSM Window.
       private static void layoutComponentsCSM() {
         JPanel box0 = new JPanel(),
                box1 = new JPanel(),
                box2 = new JPanel(),
                box3 = new JPanel();
               
         box0.setLayout(new BoxLayout(box0, BoxLayout.Y_AXIS));
         box1.setLayout(new BoxLayout(box1,BoxLayout.Y_AXIS));
         box3.setLayout(new BoxLayout(box3, BoxLayout.X_AXIS));
         frame2.add(box0);
         box0.add(box1);
         box0.add(box2);
         box0.add(box3);
         box1.setAlignmentX(Component.CENTER_ALIGNMENT);
         box3.setAlignmentX(Component.CENTER_ALIGNMENT);
         box1.setBorder(new EmptyBorder(15, 15,15, 15));
         box2.setBorder(new EmptyBorder(5, 5, 5, 5));
         box3.setBorder(new EmptyBorder(5, 5, 5, 5));
         JLabel label = new JLabel("Pending Jobs");
         label.setForeground(Color.BLACK);
         label.setFont(new Font("Serif", Font.PLAIN, 14));
         box1.add( label);
         box2.add(jobDetailCsm);
         box3.add(saveBtn);
         JScrollPane scroller2 = new JScrollPane(mylist);
         box1.add(scroller2);
         scroller2.setAlignmentX( Component.LEFT_ALIGNMENT);
         JScrollPane scroller3 = new JScrollPane(jobDetailCsm);
         box2.add(scroller3);
         scroller3.setAlignmentX( Component.LEFT_ALIGNMENT);
         jobDetailCsm.setEditable(false);
         jobDetailCsm.setText(action.toString());
      }
    
      // addListeners2() adds event listeners to the
      // CCC components and the CCC frame.
       private static void addListenersCCC() {
         leaveBtn.addActionListener(
            new ActionListener() {
               public void actionPerformed(
                  ActionEvent e) {
                  long phone = Long.parseLong(phoneNumber.getText());
                  String newName = name.getText();
                  int a=action.addOpen(newName,phone,jobDetail.getText());
                  listModel.addElement(String.format("%d",a ));
                  phoneNumber.setText("");
                  name.setText("");
                  jobDetail.setText("");
                  jobDetailTso.setText(action.toString());
                  jobDetailCsm.setText(action.toString()); 
               }
            });
         resolveBtn.addActionListener(
            new ActionListener() {
               public void actionPerformed(
                  ActionEvent e) {
                  int ph = Integer.parseInt(phoneNumber.getText());
                  action.addClose(name.getText(),ph,jobDetail.getText());
                  phoneNumber.setText("");
                  name.setText("");
                  jobDetail.setText("");
               }
            });
         frame.addWindowListener(
            new WindowAdapter(){
               public void windowClosing(
                  WindowEvent e) {
                  action.saveOpen(FILE_NAME);
                  action.saveClose(FILE_NAME2);
               }
            });
      }
     
      // addListeners() adds event listeners to the TSO
      // components and the frame.
      private static void addListenersTSO() {
         resolveBtn2.addActionListener(
            new ActionListener() {
               public void actionPerformed(
                  ActionEvent e) {
                  action.removeOpen();
                  listModel.remove(0);
                  jobDetailTso.setText(action.toString());
                  jobDetailCsm.setText(action.toString());
               }
            });
         
         frame1.addWindowListener(
            new WindowAdapter(){
               public void windowClosing(
                  WindowEvent e) {
                  action.saveOpen(FILE_NAME);
                  action.saveClose(FILE_NAME2);
               }
            });
      }
      
      private static void addListenersCSM() {
         saveBtn.addActionListener(
            new ActionListener() {
               public void actionPerformed(
                  ActionEvent e) {
                  action.saveOpen(FILE_NAME);
                  action.saveClose(FILE_NAME2);
               }
            });
         priorityBtn.addActionListener(
            new ActionListener() {
               public void actionPerformed(
                  ActionEvent e) {
                  //Do Nothing
               }
            });
         frame2.addWindowListener(
            new WindowAdapter(){
               public void windowClosing(
                  WindowEvent e) {
                     action.saveOpen(FILE_NAME);
                    action.saveClose(FILE_NAME2);
               }
            });
         MouseListener mouseListener = new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                  if (e.getClickCount() == 1) {
                        int index = mylist.locationToIndex(e.getPoint());
                        String s=mylist.getSelectedValue();
                        action.priority(s);
                        jobDetailTso.setText(action.toString());
                        String temp=listModel.getElementAt(0);
                        listModel.set(0,listModel.getElementAt(index));
                        listModel.set(index,temp);
                  }
            }
         };
            mylist.addMouseListener(mouseListener);
      }

   }
